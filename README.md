# launch

Dieses Repository enthält die Linux-Services und ROS-Launch-Files für das autonome System. Vor der Installation müssen die `brc_packages` auf dem System im Home-Ordner vorhanden sein. Nach der Installation und einem Reboot starten die Services automatisch, welche dann automatisch alle nötigen ROS-Nodes aus den `brc_packages` starten.

## TL;DR

0. Clone this repository `into ~/catkin_ws/`.
1. Run respective setup scripts.
2. Profit.

To profit further, always run setup scripts after a pull.
