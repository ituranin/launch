#!/bin/bash

export ROS_HOSTNAME=192.168.77.200
export ROS_IP=192.168.77.200
export ROS_MASTER_URI=http://192.168.77.202:11311

source /opt/ros/melodic/setup.bash
source ~/catkin_ws/devel/setup.bash
roslaunch ~/catkin_ws/launch/nvidia.launch

