#!/bin/bash

sudo cp -f /home/$USER/catkin_ws/launch/brcDspace.service /etc/systemd/system/
cp -f /home/$USER/catkin_ws/launch/runDspace.sh /home/$USER/

sudo systemctl daemon-reload
sudo systemctl enable brcDspace.service
sudo systemctl start brcDspace.service
