#!/bin/bash

sudo cp -f /home/$USER/catkin_ws/launch/brcNvidia.service /etc/systemd/system/
cp -f /home/$USER/catkin_ws/launch/runNvidia.sh /home/$USER/

sudo systemctl daemon-reload
sudo systemctl enable brcNvidia.service
sudo systemctl start brcNvidia.service
